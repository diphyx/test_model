import pandas as pd
import numpy as np
import os

import click


def model_generator(resolution, time, path):
    result = np.array([])
    for i in range(0,resolution):
        result = np.append(result, np.sin(2*np.pi/resolution*i)*np.sin(2*np.pi*time))
    print(result)
    filename = os.path.join(path, "result_"+str(time))
    result.tofile(filename, sep=',')
    # with open("result_"+str(time),"w") as f:
    #     f.write(str(result)
    # f.close()

    
@click.command()
@click.option("--res", default=10, type=int)
@click.option("--start_time", default=0, type=float)
@click.option("--end_time", required=True, type=float)
@click.option("--delta_time",required=True, type=float)
@click.option("--path",default=os.getcwd(), type=click.Path())
def main(res, start_time, end_time, delta_time, path):
    print(res)
    print(start_time, end_time, delta_time)
    for time in np.arange(start_time, end_time, delta_time):
        model_generator(res, time, path)
    
if __name__ == "__main__":
    main()
